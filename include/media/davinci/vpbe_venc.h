/*
 * Copyright (C) 2010 Texas Instruments Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef _VPBE_VENC_H
#define _VPBE_VENC_H

#include <media/v4l2-subdev.h>

#define VPBE_VENC_SUBDEV_NAME "vpbe-venc"

/* venc events */
#define VENC_END_OF_FRAME	1
#define VENC_FIRST_FIELD	2
#define VENC_SECOND_FIELD	4

struct venc_platform_data {
	enum vpbe_types venc_type;
	int (*setup_clock)(enum vpbe_enc_timings_type type,
			__u64 mode);
	/* Number of LCD outputs supported */
	int num_lcd_outputs;
};


#endif
